package com.sic.services.exceptions;

public class EmptyProdutoEstoqueBaixo extends RuntimeException {

	private static final long serialVersionUID = 323305792030946880L;

	public EmptyProdutoEstoqueBaixo(String message) {
		super(message);
	}
	
	public EmptyProdutoEstoqueBaixo(String message, Throwable causa) {
		super(message, causa);
	}
}
