package com.sic.services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sic.model.Fornecedor;
import com.sic.model.Produto;
import com.sic.model.ReputacaoFornecedorEnum;
import com.sic.repository.FornecedoresRepository;
import com.sic.repository.ProdutosRepository;
import com.sic.resource.util.ExcelUtil;

@Service
public class SICLegadoUtilService {

	@Autowired
	private FornecedoresRepository fornecedoresRepository;

	@Autowired
	private ProdutosRepository produtoRepository;

	public void iniciaBanco() throws IOException {
		// Grava Cadastro de fornecedores
		gravaFornecedores();
		// Grava Cadastro de produtos
		gravaProdutos();
	}
	

	public void gravaProdutos() throws IOException {
		List<Produto> produtos = ExcelUtil.insertMethodInterface(); 
		
		List<Fornecedor> fornecedores = this.fornecedoresRepository.findAll();

		produtos.forEach(p -> {
			p.setFornecedores(fornecedores);
			this.produtoRepository.save(p);
		});
	}

	public void gravaFornecedores() {
		// Fornecedor 1
		Fornecedor fornecedor1 = new Fornecedor();
		fornecedor1.setCnpj("40323526000127");
		fornecedor1.setNome("Fornecedor Sic MG");
		fornecedor1.setEmail("fornecedor1sic@gmail.com");
		fornecedor1.setReputacao(ReputacaoFornecedorEnum.OTIMO);
		this.fornecedoresRepository.save(fornecedor1);

		// Fornecedor 2
		Fornecedor fornecedor2 = new Fornecedor();
		fornecedor2.setCnpj("28117979000100");
		fornecedor2.setNome("Fornecedor Sic SP");
		fornecedor2.setEmail("fornecedor2spsic@gmail.com");
		fornecedor2.setReputacao(ReputacaoFornecedorEnum.BOM);
		this.fornecedoresRepository.save(fornecedor2);

		// Fornecedor 3
		Fornecedor fornecedor3 = new Fornecedor();
		fornecedor3.setCnpj("07469464000190");
		fornecedor3.setNome("Fornecedor Sic RJ");
		fornecedor3.setEmail("fornecedor3rjsic@gmail.com");
		fornecedor3.setReputacao(ReputacaoFornecedorEnum.REGULAR);
		this.fornecedoresRepository.save(fornecedor3);

		// Fornecedor 4
		Fornecedor fornecedor4 = new Fornecedor();
		fornecedor4.setCnpj("58010902000199");
		fornecedor4.setNome("Fornecedor Sic MT");
		fornecedor4.setEmail("fornecedor4mtsic@gmail.com");
		fornecedor4.setReputacao(ReputacaoFornecedorEnum.BOM);
		this.fornecedoresRepository.save(fornecedor4);

		// Fornecedor 5
		Fornecedor fornecedor5 = new Fornecedor();
		fornecedor5.setCnpj("98191125000140");
		fornecedor5.setNome("Fornecedor Sic BA");
		fornecedor5.setEmail("fornecedor5basic@gmail.com");
		fornecedor5.setReputacao(ReputacaoFornecedorEnum.OTIMO);
		this.fornecedoresRepository.save(fornecedor5);
	}
}
