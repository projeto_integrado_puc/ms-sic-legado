package com.sic.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sic.model.Fornecedor;
import com.sic.repository.FornecedoresRepository;

@Service
public class FornecedorService {

	@Autowired
	private FornecedoresRepository forneceRepository;

	public List<Fornecedor> findAll() {
		return this.forneceRepository.findAll();
	}
	
	public Optional<Fornecedor> find(Long id){
		return this.forneceRepository.findById(id);
	}
}
