package com.sic.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.sic.model.Fornecedor;
import com.sic.model.Produto;
import com.sic.model.dto.FornecedorDTO;
import com.sic.model.dto.ProdutoDTO;
import com.sic.repository.ProdutosRepository;
import com.sic.services.exceptions.EmptyProdutoEstoqueBaixo;


@Service
public class ProdutoService {
	
	@Autowired
	private ProdutosRepository produtoRepositorio;
	
	public List<ProdutoDTO> produtosSemEstoque(){
		List<ProdutoDTO> response =  new ArrayList<>();
		List<Produto> produtos = this.produtoRepositorio.findAllByOrderByDescricao();
		
		if(Objects.nonNull(produtos) && !produtos.isEmpty()) {
			Map<Long, FornecedorDTO> mapFornecedores = new HashMap<>();
			for(Produto p : produtos) {
				if(p.isEstoqueMinimo() && (Objects.isNull(p.isCotacaoEmAberto()) || !p.isCotacaoEmAberto())) {
					ProdutoDTO produto = new ProdutoDTO(p.getId(), p.getDescricao(), p.getCodigo());
					produto.setFornecedores(new ArrayList<>());
					if(!CollectionUtils.isEmpty(p.getFornecedores())) {
						for(Fornecedor f : p.getFornecedores()) {
							if(mapFornecedores.containsKey(f.getId())) {
								produto.getFornecedores().add(mapFornecedores.get(f.getId()));
							}else {
								FornecedorDTO fornecedor = new FornecedorDTO();
								fornecedor.setCnpj(f.getCnpj());
								fornecedor.setEmail(f.getEmail());
								fornecedor.setId(f.getId());
								fornecedor.setNome(f.getNome());
								fornecedor.setReputacao(f.getReputacao());
								produto.getFornecedores().add(fornecedor);
							}		
						}
					}
					response.add(produto);
				}
			}
		}
		return response;
	}
	
	public List<Produto> findByDescricaoAndEmEstoqueMinimo(String descricao, String emEstoqueMinimo) {
		List<Produto> produtos = this.produtoRepositorio.findByDescricaoAndEstoqueMinimo(descricao, emEstoqueMinimo);
		if(Objects.nonNull(produtos) && !produtos.isEmpty()) {
			return produtos;
		}else {
			throw new EmptyProdutoEstoqueBaixo("Não encontrado produtos com estoque minimo.");
		}
	}

	@Transactional
	public void updateCotacao(List<Long> produtos){
		if(Objects.nonNull(produtos)) {
			produtos.forEach(p -> {
				this.produtoRepositorio.setCotacaoEmAbertoById(p);
			});
		}
	}
	
	public Optional<Produto> find(Long id){
		return this.produtoRepositorio.findById(id);
	}
}
