package com.sic.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sic.model.DetalhesErro;
import com.sic.services.exceptions.EmptyProdutoEstoqueBaixo;

@ControllerAdvice
public class ResourceExceptionHandler {
	
	@ExceptionHandler(EmptyProdutoEstoqueBaixo.class)
	public ResponseEntity<DetalhesErro> HandlerAcessoNegado(EmptyProdutoEstoqueBaixo e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(204l);
		erro.setTitulo("Não existem produtos com estoque abaixo do mínimo.");
		erro.setMensagemDesenvolvedor("Não existem produtos com estoque abaixo do mínimo.");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(erro);
	}
}