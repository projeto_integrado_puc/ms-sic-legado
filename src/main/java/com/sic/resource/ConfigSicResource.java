package com.sic.resource;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.services.SICLegadoUtilService;

@RestController
@CrossOrigin
@RequestMapping("/config")
public class ConfigSicResource {

	@Autowired
	private SICLegadoUtilService sicLegadoUtilService;
	
	@RequestMapping(method = RequestMethod.GET)
	public void iniciarBD() throws IOException{
		sicLegadoUtilService.iniciaBanco();
		System.out.println("Banco inicializado.");
	}
}
