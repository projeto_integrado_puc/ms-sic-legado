package com.sic.resource.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import com.sic.model.Produto;

public class ExcelUtil {

	public static List<Produto> insertMethodInterface() throws IOException {
		List<Produto> produtos = new ArrayList<Produto>();
		try {
			FileInputStream arquivo = new FileInputStream(new File("produtos.xls"));

			HSSFWorkbook workbook = new HSSFWorkbook(arquivo);

			HSSFSheet sheetAlunos = workbook.getSheetAt(0);

			Iterator<Row> rowIterator = sheetAlunos.iterator();

			int cont = 1;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(Objects.isNull(row.getCell(0)) || "".equals(row.getCell(0).getStringCellValue())) {
					break;
				}
				Produto produto = new Produto();
				produto.setCodigo(row.getCell(1).getStringCellValue());
				produto.setDescricao(row.getCell(0).getStringCellValue());
				produto.setEstoqueMinimo(cont%3 == 0);
				produto.setCotacaoEmAberto(false);
				produtos.add(produto);
				cont++;				
			}
			arquivo.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel n�o encontrado!");
		}
		return produtos;
	}
}
