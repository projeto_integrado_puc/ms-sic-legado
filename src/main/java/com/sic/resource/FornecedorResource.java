package com.sic.resource;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.Fornecedor;
import com.sic.services.FornecedorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Fornecedor")
@RestController
@CrossOrigin
@RequestMapping("/fornecedor")
public class FornecedorResource {

	@Autowired
	private FornecedorService fornecedorService;
	
	@ApiOperation("Lista todos os fornecedores")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Fornecedor>> listar(){
		List<Fornecedor> fornecedores  = fornecedorService.findAll();
		
		if(Objects.nonNull(fornecedores) && !fornecedores.isEmpty()) {
			return ResponseEntity.ok(fornecedores);
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ApiOperation("Retornar um fornecedor de acordo com o id")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable("id") Long id) {
		Optional<Fornecedor> opFornecedor  = fornecedorService.find(id);
		
		if(opFornecedor.isPresent()) {
			return ResponseEntity.ok(opFornecedor.get());
		}else {
			return ResponseEntity.noContent().build();
		}
	}
}
