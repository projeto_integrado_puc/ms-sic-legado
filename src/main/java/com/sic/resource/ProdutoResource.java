package com.sic.resource;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.Produto;
import com.sic.model.dto.ProdutoDTO;
import com.sic.services.ProdutoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Produto")
@RestController
@CrossOrigin
@RequestMapping("/produto")
public class ProdutoResource {
	
	@Autowired
	private ProdutoService produtoService;
	
	@ApiOperation("Lista todos os produtos")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> listar(){
		List<ProdutoDTO> produtos  = produtoService.produtosSemEstoque();
		
		if(Objects.nonNull(produtos) && !produtos.isEmpty()) {
			return ResponseEntity.ok(produtos);
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ApiOperation("Retorna um produto")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Long id) {
		Optional<Produto> opProduto  = produtoService.find(id);
		
		if(opProduto.isPresent()) {
			return ResponseEntity.ok(opProduto.get());
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ApiOperation("Atualiza o status em cotação do produto")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizaItensEmCotacao(@RequestBody List<Long> produtos){
		if(Objects.nonNull(produtos) && produtos.size() > 0) {
			this.produtoService.updateCotacao(produtos);
		}
		return ResponseEntity.ok().build();
	}
}
