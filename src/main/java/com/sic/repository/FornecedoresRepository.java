package com.sic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sic.model.Fornecedor;

public interface FornecedoresRepository extends JpaRepository<Fornecedor, Long> {
		
}