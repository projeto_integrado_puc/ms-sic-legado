package com.sic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sic.model.Produto;

public interface ProdutosRepository extends JpaRepository<Produto, Long> {
		
	public List<Produto> findByDescricaoAndEstoqueMinimo(String descricao, String estoqueMinimo);
	
	public List<Produto> findAllByOrderByDescricao();
	
	@Modifying
	@Query("update Produto p set p.cotacaoEmAberto = true where p.id = :id")
	void setCotacaoEmAbertoById(@Param("id") Long id);
}
