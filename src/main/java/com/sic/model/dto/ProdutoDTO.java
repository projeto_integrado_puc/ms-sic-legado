package com.sic.model.dto;

import java.io.Serializable;
import java.util.List;

public class ProdutoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;

	private String descricao;

	private String codigo;
	
	private List<FornecedorDTO> fornecedores;
	
	public ProdutoDTO() {
		super();
	}

	public ProdutoDTO(Long id, String descricao, String codigo) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<FornecedorDTO> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<FornecedorDTO> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}