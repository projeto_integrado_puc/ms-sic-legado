package com.sic.model.dto;

import java.io.Serializable;

import com.sic.model.ReputacaoFornecedorEnum;

public class FornecedorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String cnpj;
	private ReputacaoFornecedorEnum reputacao;
	private String email;
	
	public FornecedorDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public ReputacaoFornecedorEnum getReputacao() {
		return reputacao;
	}

	public void setReputacao(ReputacaoFornecedorEnum reputacao) {
		this.reputacao = reputacao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
