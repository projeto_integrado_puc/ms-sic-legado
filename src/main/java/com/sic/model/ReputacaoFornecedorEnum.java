package com.sic.model;

public enum ReputacaoFornecedorEnum {

	REGULAR(1, "Regular"), BOM(2, "Bom"), OTIMO(3, "Ótimo");
	
	private int valor;
	private String descricao;
	
	private ReputacaoFornecedorEnum(int valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}
	
	public int getValor() {
		return valor;
	}
	public String getDescricao() {
		return descricao;
	}
}