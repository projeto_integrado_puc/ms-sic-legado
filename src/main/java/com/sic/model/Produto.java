package com.sic.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "TBPRODUTO")
@DynamicInsert
@DynamicUpdate
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String descricao;

	@NotEmpty
	private String codigo;

	private boolean estoqueMinimo;
	
	private boolean cotacaoEmAberto;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Fornecedor> fornecedores;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public boolean isEstoqueMinimo() {
		return estoqueMinimo;
	}

	public void setEstoqueMinimo(boolean estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}

	public boolean isCotacaoEmAberto() {
		return cotacaoEmAberto;
	}

	public void setCotacaoEmAberto(boolean cotacaoEmAberto) {
		this.cotacaoEmAberto = cotacaoEmAberto;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}
}
