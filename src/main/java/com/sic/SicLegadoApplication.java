package com.sic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SicLegadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SicLegadoApplication.class, args);
	}

}
